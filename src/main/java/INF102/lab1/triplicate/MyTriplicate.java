package INF102.lab1.triplicate;

import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        int n = list.size();

        // Double loop to find a triplicate element
        for (int i = 0; i < n; i++) {
            T current = list.get(i); // Get the current element at index i
            int count = 0; // Initialize a counter for the current element's occurrences

            // Loop through the rest of the list to compare with the current element
            for (int j = i; j < n; j++) {
                if (list.get(j).equals(current)) { // Check if the elements are equal
                    count++; // Increment the counter if the elements are equal

                    if (count >= 3) { // If the counter reaches 3 or more
                        return current; // Return the current element as a triplicate
                    }
                }
            }
        }

        // If no triplicate is found, return null
        return null;
    }
}
